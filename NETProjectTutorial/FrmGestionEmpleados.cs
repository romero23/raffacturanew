﻿using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleados : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        private EmpleadoModel empleadoModel;

        public DataSet DsEmpleados
        {
            get
            {
                return dsEmpleados;
            }

            set
            {
                dsEmpleados = value;
            }
        }

        public FrmGestionEmpleados()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
            empleadoModel = new EmpleadoModel();
        }

        private void TxtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsEmpleados.Filter = string.Format("Cédula like '*{0}*' or Nombres like '*{0}*' or Apellidos like '*{0}*'", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = DsEmpleados;
            bsEmpleados.DataMember = DsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }


        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmEmpleado fp = new FrmEmpleado(); 
            fp.TblEmpleados = DsEmpleados.Tables["Empleado"];
            fp.DsEmpleados = DsEmpleados;
            fp.ShowDialog();    
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmEmpleado fp = new FrmEmpleado();
            fp.TblEmpleados = DsEmpleados.Tables["Empleado"];
            fp.DsEmpleados = DsEmpleados;
            fp.DrEmpleados = drow;
            fp.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsEmpleados.Tables["Empleado"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void FrmGestionEmpleados_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable dtEmpleadoAdded = dsEmpleados.Tables["Empleado"].GetChanges(DataRowState.Added);
            DataTable dtEmpleadoUpdated = dsEmpleados.Tables["Empleado"].GetChanges(DataRowState.Modified);
            DataTable dtEmpleadoDeleted = dsEmpleados.Tables["Empleado"].GetChanges(DataRowState.Deleted);

            if (dtEmpleadoAdded != null)
            {
                foreach (DataRow dr in dtEmpleadoAdded.Rows)
                {

                    empleadoModel.save(dr);
                    dsClientes.Tables["Cliente"].Rows.Find(dr["Id"]).AcceptChanges();
                }

            }

            if (dtClientesUpdated != null)
            {

            }

            if (dtClientesDeleted != null)
            {

            }
        }
    }
}
