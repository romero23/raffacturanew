﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionFacturas : Form
    {
        private DataSet dsFacturas;
        private BindingSource bsFacturas;

        public DataSet DsFacturas
        {
            get
            {
                return dsFacturas;
            }

            set
            {
                dsFacturas = value;
            }
        }

        public FrmGestionFacturas()
        {
            InitializeComponent();
            bsFacturas = new BindingSource();
        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsFacturas.Filter = string.Format("CodFactura like '*{0}*' or Empleado like '*{0}*'", txtFinder.Text);
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FrmGestionFactura_Load(object sender, EventArgs e)
        {
            bsFacturas.DataSource = DsFacturas;
            bsFacturas.DataMember = DsFacturas.Tables["Factura"].TableName;
            dgvFacturas.DataSource = bsFacturas;
            dgvFacturas.AutoGenerateColumns = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.TblFacturas = dsFacturas.Tables["Factura"];
            ff.DsSistema = dsFacturas;
            ff.ShowDialog();
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvFacturas.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder ver", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmReporteFactura frf = new FrmReporteFactura();
            frf.DsSistema = DsFacturas;
            frf.DrFactura = drow;
            frf.Show();
        }
    }
}
