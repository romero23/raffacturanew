﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.implements
{
    class DaoImplemenProducto : IDAOProducto
    {

        //header cliente
        private BinaryReader brhproducto;
        private BinaryWriter bwhproducto;
        //data cliente
        private BinaryReader brdproducto;
        private BinaryWriter bwdproducto;

        private FileStream fshproducto;
        private FileStream fsdproducto;

        private const string FILENAME_HEADER = "hproducto.dat";
        private const string FILENAME_DATA = "dproducto.dat";
        private const int SIZE = 390;

        public DaoImplemenProducto() { }

        private void open()
        {
            try
            {
                fsdproducto = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshproducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhproducto = new BinaryReader(fshproducto);
                    bwhproducto = new BinaryWriter(fshproducto);

                    brdproducto = new BinaryReader(fsdproducto);
                    bwdproducto = new BinaryWriter(fsdproducto);

                    bwhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhproducto.Write(0);//n
                    bwhproducto.Write(0);//k
                }
                else
                {
                    fshproducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhproducto = new BinaryReader(fshproducto);
                    bwhproducto = new BinaryWriter(fshproducto);
                    brdproducto = new BinaryReader(fsdproducto);
                    bwdproducto = new BinaryWriter(fsdproducto);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdproducto != null)
                {
                    brdproducto.Close();
                }
                if (brhproducto != null)
                {
                    brhproducto.Close();
                }
                if (bwdproducto != null)
                {
                    bwdproducto.Close();
                }
                if (bwhproducto != null)
                {
                    bwhproducto.Close();
                }
                if (fsdproducto != null)
                {
                    fsdproducto.Close();
                }
                if (fshproducto != null)
                {
                    fshproducto.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public Producto findById(int id)
        {
            throw new NotImplementedException();
        }

        public Producto findBySku(string sku)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findByname(string name)
        {
            throw new NotImplementedException();
        }

        public void save(Producto t)
        {
            open();
            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();
            int k = brhproducto.ReadInt32();

            long dpos = k * SIZE;
            bwdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdproducto.Write(++k);
            bwdproducto.Write(t.Sku);
            bwdproducto.Write(t.Nombre);
            bwdproducto.Write(t.Descripcion);
            bwdproducto.Write(t.Cantidad);
            bwdproducto.Write(t.Precio);
            

            bwhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhproducto.Write(++n);
            bwhproducto.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhproducto.Write(k);
            close();
        }

        public int update(Producto t)
        {
            
        }

        public bool delete(Producto t)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findAll()
        {
            open();
            List<Producto> producto = new List<Producto>();

            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhproducto.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdproducto.ReadInt32();
                string sku = brdproducto.ReadString();
                string nombres = brdproducto.ReadString();
                string descripcion = brdproducto.ReadString();
                int cantidad = brdproducto.ReadInt32();
                double precio = brdproducto.ReadDouble();
                Producto c = new Producto(id, sku, nombres,
                    descripcion,cantidad,precio);
                producto.Add(c);
            }

            close();
            return producto;
        }
    }
}
